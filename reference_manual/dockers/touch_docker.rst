.. meta::
   :description:
        Overview of the touch docker.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
   :license: GNU free documentation license 1.3 or later.

.. index:: Touch, Finger, Tablet UI
.. _touch_docker:

============
Touch Docker
============

The Touch Docker is a QML docker with several convenient actions on it. Its purpose is to aid those who use Krita on a touch-enabled screen by providing bigger gui elements.

Its actions are... 

+------------------------------+-----------------------+----------------------+
|         Open File            |       Save File       |        Save As...    |
+------------------------------+-----------+-----------+----------------------+
|                 Undo                     |               Redo               |
+------------------------------------------+----------------------------------+
|                                  Decrease Opacity                           |
+-----------------------------------------------------------------------------+
|                                  Increase Opacity                           |
+-----------------------------------------------------------------------------+
|                                 Increase Lightness                          |
+-----------------------------------------------------------------------------+
|                                 Decrease Lightness                          |
+------------------------------+-----------------------+----------------------+
|                              |        Zoom in        |                      |
+------------------------------+-----------------------+----------------------+
| Rotate Counter Clockwise 15° | Reset Canvas Rotation | Rotate Clockwise 15° |
+------------------------------+-----------------------+----------------------+
|                              |        Zoom out       |                      |
+------------------------------+-----------------------+----------------------+
|                                 Decrease Brush Size                         |
+-----------------------------------------------------------------------------+
|                                 Increase Brush Size                         |
+-----------------------------------------------------------------------------+
|                                Delete Layer Contents                        |
+-----------------------------------------------------------------------------+
