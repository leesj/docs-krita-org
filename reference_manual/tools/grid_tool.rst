.. meta::
   :description:
        Krita's grid tool reference.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Scott Petrovic
   :license: GNU free documentation license 1.3 or later.

.. index:: Tools, Grid
.. _grid_tool:

=========
Grid Tool
=========

|toolgrid|

.. deprecated:: 3.0

    Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead.

When you click on the edit grid tool, you'll get a message saying that to activate the grid you must press the :kbd:`Enter` key.
Press the :kbd:`Enter` key to make the grid visible. Now you must have noticed the tool icon for your pointer has changed to icon similar to move tool.

To change the spacing of the grid, press and hold the :kbd:`Ctrl` key and then the |mouseleft| :kbd:`+ drag` shortcut on the canvas. In order to move the grid you have to press the :kbd:`Alt` key and then the |mouseleft| :kbd:`+ drag` shortcut.
