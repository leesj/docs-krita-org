# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:27+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: boudewijnrempt krita woltheralaptop DKE chat Cooksley\n"
"X-POFile-SpellExtra: Krayon sprints Foundation Reddit Relay bcooksley Docs\n"
"X-POFile-SpellExtra: boud CET Facebook CEST dmitryklog kimageshop irc\n"
"X-POFile-SpellExtra: Github Ben Mastodon Pepper Bugzilla Wong Wolthera\n"
"X-POFile-SpellExtra: Dmitry Revoy disruptivo Alvin UX Phabricator Krita\n"
"X-POFile-SpellExtra: scottyp Twitter deevad Tumblr windragon Carrot\n"
"X-POFile-SpellExtra: Petrovic Chat Deventer Boudewijn ref Gitlab\n"

#: ../../contributors_manual/community.rst:1
msgid "Guide to the Krita community"
msgstr "Guia para a Comunidade do Krita"

#: ../../contributors_manual/community.rst:10
msgid "community"
msgstr "comunidade"

#: ../../contributors_manual/community.rst:10
msgid "communication"
msgstr "comunicação"

#: ../../contributors_manual/community.rst:16
msgid "The Krita Community"
msgstr "A Comunidade do Krita"

#: ../../contributors_manual/community.rst:18
msgid ""
"Get in touch! Apart from the website at https://www.krita.org, the Krita "
"project has three main communication channels:"
msgstr ""
"Entre em contacto! À parte da página Web em https://www.krita.org, o "
"projecto Krita tem três canais de comunicação principais:"

#: ../../contributors_manual/community.rst:20
msgid "Internet Relay Chat (IRC)"
msgstr "Internet Relay Chat (IRC)"

#: ../../contributors_manual/community.rst:21
msgid "The mailing list"
msgstr "A lista de correio"

#: ../../contributors_manual/community.rst:22
#: ../../contributors_manual/community.rst:86
msgid "Phabricator"
msgstr "Phabricator"

#: ../../contributors_manual/community.rst:24
msgid ""
"While Krita developers and users are present on social media such as "
"Twitter, Mastodon, Reddit, Google+, Tumblr or Facebook, those are not the "
"place where we discuss new features, bugs, development or where we make "
"plans for the future."
msgstr ""
"Embora os programadores do Krita estejam presentes nas redes sociais, como o "
"Twitter, Mastodon, Reddit, Google+, Tumblr ou Facebook, esses não são os "
"locais onde são discutidos erros, novas funcionalidades, informação de "
"desenvolvimento ou qualquer espécie de planeamento futuro."

#: ../../contributors_manual/community.rst:26
msgid "There are also the:"
msgstr "Existe também:"

#: ../../contributors_manual/community.rst:28
msgid "bug tracker"
msgstr "o sistema de registo de erros"

#: ../../contributors_manual/community.rst:29
msgid "development sprints"
msgstr "etapas ('sprints') de desenvolvimento"

#: ../../contributors_manual/community.rst:31
msgid ""
"You’ll find that there are a number of people are almost always around: the "
"core team."
msgstr ""
"Irá descobrir que existem algumas pessoas quase sempre por aí: a equipa de "
"base."

#: ../../contributors_manual/community.rst:33
msgid ""
"Boudewijn (irc: boud): project maintainer, lead developer. Works full-time "
"on Krita. Manages the Krita Foundation, triages bugs, does social media and "
"admin stuff. Boudewijn is also on Reddit as boudewijnrempt."
msgstr ""
"Boudewijn (irc: boud): manutenção do projecto, chefe de desenvolvimento. "
"Trabalha a tempo inteiro no Krita. Gestor da Krita Foundation, faz a triagem "
"de erros, faz algumas tarefas de comunicação social e de administração do "
"sistema. O Boudewijn também está presente no Reddit como boudewijnrempt."

#: ../../contributors_manual/community.rst:34
msgid "Dmitry (irc: dmitryk_log): lead developer. Works full-time on Krita."
msgstr ""
"Dmitry (irc: dmitryk_log): chefe de desenvolvimento. Trabalha a tempo "
"inteiro no Krita."

#: ../../contributors_manual/community.rst:35
msgid ""
"Wolthera (irc: wolthera_laptop): developer, writes the manual and tutorials, "
"triages bugs, helps people out"
msgstr ""
"Wolthera (irc: wolthera_laptop): desenvolvimento, escreve o manual e os "
"tutoriais, faz a triagem dos erros, ajuda as pessoas"

#: ../../contributors_manual/community.rst:36
msgid "Scott Petrovic (irc: scottyp): UX designer, developer, webmaster"
msgstr ""
"Scott Petrovic (irc: scottyp): desenho e UX, desenvolvimento, administrador "
"Web"

#: ../../contributors_manual/community.rst:37
msgid ""
"David Revoy (irc: deevad): expert user, creates Pepper & Carrot, maintains "
"the preset bundle."
msgstr ""
"David Revoy (irc: deevad): utilizador experiente, cria o Pepper & Carrot, "
"mantém o pacote de predefinições."

#: ../../contributors_manual/community.rst:38
msgid "Alvin Wong (irc: windragon): windows guru"
msgstr "Alvin Wong (irc: windragon): guru do Windows"

#: ../../contributors_manual/community.rst:39
msgid "Ben Cooksley (irc: bcooksley): KDE system administrator."
msgstr "Ben Cooksley (irc: bcooksley): administrador de sistemas do KDE."

#: ../../contributors_manual/community.rst:41
msgid ""
"Krita’s team spans the globe, but most development happens in Europe and "
"Russia."
msgstr ""
"A equipa do Krita está espalhada em todo o globo, mas a maior parte do "
"desenvolvimento acontece na Europa e na Rússia."

#: ../../contributors_manual/community.rst:43
msgid ""
"Krita is part of the larger KDE community. The KDE® Community is a free "
"software community dedicated to creating an open and user-friendly computing "
"experience, offering an advanced graphical desktop, a wide variety of "
"applications for communication, work, education and entertainment and a "
"platform to easily build new applications upon. The KDE contributors guide "
"is relevant for Krita contributors, too, and can be found `here <https://"
"archive.flossmanuals.net/kde-guide/>`_."
msgstr ""
"O Krita faz parte da grande comunidade do KDE. A Comunidade do KDE® é uma "
"comunidade de 'software livre' que está dedicada a criar uma experiência "
"informática aberta e amigável, oferecendo um ambiente de trabalho gráfico "
"avançado, uma grande variedade de aplicações para comunicações, trabalho, "
"educação e entretenimento, bem como uma plataforma sobre a qual desenvolver "
"novas aplicações. O guia de contribuintes do KDE é relevante também para os "
"colaboradores do Krita, e poderá ser lido `aqui <https://archive."
"flossmanuals.net/kde-guide/>`_."

#: ../../contributors_manual/community.rst:45
msgid ""
"The Krita Foundation was created to support development of Krita. The Krita "
"Foundation has sponsored Dmitry’s work on Krita since 2013."
msgstr ""
"A Krita Foundation foi criada para dar suporte ao desenvolvimento do Krita. "
"A Krita Foundation patrocinou o trabalho do Dmitry no Krita desde 2013."

#: ../../contributors_manual/community.rst:48
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat"

#: ../../contributors_manual/community.rst:50
msgid ""
"IRC is the main communication channel. There are IRC clients for every "
"operating system out there, as well as a web client on the krita website."
msgstr ""
"O IRC é o canal de comunicação principal. Existem clientes de IRC para todos "
"os sistemas operativos por aí, assim como um cliente Web na página Web do "
"Krita."

#: ../../contributors_manual/community.rst:52
msgid ""
"Joining IRC: connect to irc.freenode.net, select a unique nickname and join "
"the #krita and ##krita-chat channels. #krita is for on-topic talk, ##krita-"
"chat for off-topic chat."
msgstr ""
"Juntar-se ao IRC: ligue-se a irc.freenode.net, seleccione uma alcunha única "
"e junte-se aos canais #krita e ##krita-chat.. O #krita é para conversas "
"sobre um tópico, enquanto o ##krita-chat serve para conversas fora dos "
"tópicos."

#: ../../contributors_manual/community.rst:53
msgid "Don’t ask to ask: if you’ve got a question, just ask it."
msgstr "Não peça se pode perguntar: se tiver uma pergunta, basta fazê-la."

#: ../../contributors_manual/community.rst:54
msgid ""
"Don’t panic if several discussions happen at the same time. That’s normal in "
"a busy channel."
msgstr ""
"Não entre em pânico se estiverem várias discussões ao mesmo tempo. Isso é "
"normal num canal ocupado."

#: ../../contributors_manual/community.rst:55
msgid "Talk to an individual by typing their nick and a colon."
msgstr ""
"Fale com alguém individualmente, escrevendo a alcunha dele e dois-pontos."

#: ../../contributors_manual/community.rst:56
msgid ""
"Almost every Monday, at 14:00 CET or CEST, we have a meeting where we "
"discuss what happened in the past week, what we’re doing, and everything "
"that’s relevant for the project. The meeting notes are kept in google docs."
msgstr ""
"Quase todas as Segundas-Feiras, às14:00 CET ou CEST, decorre uma reunião "
"onde é discutido o que se passou na semana passada, o que estamos a fazer e "
"tudo o que é relevante para o projecto. As actas da reunião são guardadas no "
"Google Docs."

#: ../../contributors_manual/community.rst:57
msgid ""
"Activity is highest in CET or CEST daytime and evenings. US daytime and "
"evenings are most quiet."
msgstr ""
"A actividade está ao máximo durante o dia (no fuso-horário CET ou CEST) e à "
"noite. As manhãs e noites dos EUA são muito sossegadas."

#: ../../contributors_manual/community.rst:58
msgid ""
"**IRC is not logged. If you close the channel, you will be gone, and you "
"will not be able to read what happened when you join the channel again. If "
"you ask a question, you have to stay around!**"
msgstr ""
"**O IRC não guarda registos históricos. Se fechar o canal, toda a informação "
"perder-se-á, não sendo mais capaz de ler o que se passou quando se juntar ao "
"canal de novo. Se fizer alguma pergunta, terá de tomar notas ou permanecer "
"ligado!**"

#: ../../contributors_manual/community.rst:59
msgid ""
"It is really irritating for other users and disrupting to conversations if "
"you keep connecting and disconnecting."
msgstr ""
"É realmente irritante para os outros utilizadores e disruptivo para as "
"conversas se continuar a ligar-se e desligar-se."

#: ../../contributors_manual/community.rst:63
msgid "Mailing List"
msgstr "Lista de E-mail"

#: ../../contributors_manual/community.rst:65
msgid ""
"The mailing list is used for announcements and sparingly for discussions. "
"Everyone who wants to work on Krita one way or another should be subscribed "
"to the mailing list."
msgstr ""
"A lista de correio é usada para os anúncios e raramente para discussões. "
"Todos os que quiserem trabalhar no Krita, de uma forma ou outra, deverão "
"estar inscritos na lista de correio."

#: ../../contributors_manual/community.rst:67
msgid ""
"`Mailing List Archives <https://mail.kde.org/mailman/listinfo/kimageshop>`_"
msgstr ""
"Arquivo das Listas de Correio <https://mail.kde.org/mailman/listinfo/"
"kimageshop>`_"

#: ../../contributors_manual/community.rst:69
msgid ""
"The mailing list is called \"kimageshop\", because that is the name under "
"which the Krita project was started. Legal issues (surprise!) led to two "
"renames, once to Krayon, then to Krita."
msgstr ""
"A lista de correio chama-se \"kimageshop\", porque esse é o nome pelo qual "
"foi iniciado o projecto Krita. Questões legais (surpresa!) conduziram a duas "
"mudanças de nome, uma para Krayon e outra para Krita."

#: ../../contributors_manual/community.rst:73
msgid "Gitlab (KDE Invent)"
msgstr "fiabilidade "

#: ../../contributors_manual/community.rst:75
msgid "Gitlab serves the following purposes for the Krita team:"
msgstr "O Gitlab serve os seguintes propósitos para a equipa do Krita:"

#: ../../contributors_manual/community.rst:77
msgid ""
"Review volunteers' submissions: https://invent.kde.org/kde/krita/"
"merge_requests for the code itself, https://invent.kde.org/websites/docs-"
"krita-org/merge_requests for the content of the Krita Manual."
msgstr ""
"Rever os envios dos voluntários: https://invent.kde.org/kde/krita/"
"merge_requests para o código em si, https://invent.kde.org/websites/docs-"
"krita-org/merge_requests para o conteúdo do Manual do Krita."

#: ../../contributors_manual/community.rst:78
msgid ""
"Host the code git repository: https://invent.kde.org/kde/krita . Note that "
"while there are mirrors of our git repository on Github and Phabricator, we "
"do not use them for Krita development."
msgstr ""
"Alojar o repositório do Git: https://invent.kde.org/kde/krita/. Lembre-se "
"que, embora exista uma réplica do nosso repositório do Git no Github, o "
"mesmo não é usado para o desenvolvimento do Krita."

#: ../../contributors_manual/community.rst:79
msgid ""
"Host the Krita Manual content repository: https://invent.kde.org/websites/"
"docs-krita-org"
msgstr ""
"Alojar o repositório do conteúdo do Manual do Krita: https://invent.kde.org/"
"websites/docs-krita-org"

#: ../../contributors_manual/community.rst:81
msgid "**Do not** make new issues on Gitlab or use it to make bug reports."
msgstr ""
"**Não** crie novos problemas no Gitlab ou o use para criar relatórios de "
"erros."

#: ../../contributors_manual/community.rst:83
msgid ""
"**Do** put all your code submissions (merge requests) on Gitlab. **Do not** "
"attach patches to bugs in the bug tracker."
msgstr ""
"**Coloque** todas as suas alterações de código (pedidos de reunião) no "
"Gitlab. **Não** anexe as modificações aos erros no sistema de gestão de "
"erros."

#: ../../contributors_manual/community.rst:88
msgid "Phabricator serves the following purposes for the Krita team:"
msgstr "O Phabricator serve os seguintes propósitos para a equipa do Krita:"

#: ../../contributors_manual/community.rst:90
msgid ""
"Track what we are working on: https://phabricator.kde.org/maniphest/ This "
"includes development tasks, designing new features and UX design, as well as "
"tasks related to the website."
msgstr ""
"Registar o que estamos a fazer: https://phabricator.kde.org/maniphest/ Isto "
"inclui as tarefas de desenvolvimento, o desenho de novas funcionalidades e "
"desenho da interface, assim como tarefas relacionadas com a página Web."

#: ../../contributors_manual/community.rst:92
msgid ""
"**Do not** report bugs as tasks on Phabricator. Phabricator is where we "
"organize our work."
msgstr ""
"**Não** comunique erros como tarefas no Phabricator. O Phabricator é onde "
"nós organizamos o nosso trabalho."

#: ../../contributors_manual/community.rst:95
msgid "Bugzilla: the Bug Tracker"
msgstr "Bugzilla: o Sistema de Registo de Erros"

#: ../../contributors_manual/community.rst:97
msgid ""
"Krita shares the bug tracker with the rest of the KDE community. Krita bugs "
"are found under the Krita product. There are two kinds of reports in the bug "
"tracker: bugs and wishes. See the chapters on :ref:`Bug Reporting "
"<bugs_reporting>` and :ref:`Bug Triaging <triaging_bugs>` on how to handle "
"bugs. Wishes are feature requests. Do not report feature requests in "
"bugzilla unless a developer has asked you to. See the chapter on :ref:"
"`Feature Requests <developing_features>` for what is needed to create a good "
"feature request."
msgstr ""
"O Krita partilha o sistema de gestão de erros com o resto da comunidade do "
"DKE. Os erros do Krita podem ser encontrados debaixo do produto Krita. "
"Existem dois tipos de relatórios no sistema: erros e pedidos. Veja os "
"capítulos sobre a :ref:`Comunicação de Erros <bugs_reporting>` e a :ref:"
"`Triagem dos Erros <triaging_bugs>` para saber como tratar deles. Os pedidos "
"são solicitações de funcionalidades. Não comunique pedidos de "
"funcionalidades no Bugzilla, a menos que um programador lho tenha pedido. "
"Veja o capítulo sobre :ref:`Pedidos de Funcionalidades "
"<developing_features>` para saber o que é necessário para criar um novo "
"pedido de funcionalidades."

#: ../../contributors_manual/community.rst:100
msgid "Sprints"
msgstr "Etapas"

#: ../../contributors_manual/community.rst:102
msgid ""
"Sometimes, core Krita developers and users come together, most often in "
"Deventer, the Netherlands, to work together on our code design, UX design, "
"the website or whatever needs real, face-to-face contact. Travel to sprints "
"is usually funded by KDE e.V., while accommodation is funded by the Krita "
"Foundation."
msgstr ""
"Algumas vezes, os programadores de base do Krita e os utilizadores juntam-"
"se, frequentemente em Deventer, na Holanda, para trabalhar em conjunto no "
"nosso desenho do código, desenho de UX, na página Web ou em tudo o que possa "
"necessitar de contacto cara-a-cara. As viagens para esses 'sprints' "
"normalmente é financiada pela KDE e.V., enquanto o alojamento é financiado "
"pela Krita Foundation."
