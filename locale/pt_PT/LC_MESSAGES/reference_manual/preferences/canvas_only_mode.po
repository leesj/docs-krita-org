# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:17+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita Tab kbd\n"

#: ../../reference_manual/preferences/canvas_only_mode.rst:1
msgid "Canvas only mode settings in Krita."
msgstr "Configurações do modo apenas de área de desenho no Krita."

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
#: ../../reference_manual/preferences/canvas_only_mode.rst:16
msgid "Canvas Only Mode"
msgstr "Modo Apenas da Área de Desenho"

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Preferences"
msgstr "Preferências"

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/preferences/canvas_only_mode.rst:18
msgid ""
"Canvas Only mode is Krita's version of full screen mode. It is activated by "
"hitting the :kbd:`Tab` key on the keyboard. Select which parts of Krita will "
"be hidden in canvas-only mode -- The user can set which UI items will be "
"hidden in canvas-only mode. Selected items will be hidden."
msgstr ""
"O modo Apenas de Área de Desenho é a versão do Krita do modo de ecrã "
"completo. O mesmo é activado se carregar em :kbd:`Tab` no teclado. "
"Seleccione quais os componentes do Krita que ficarão escondidos no modo "
"apenas da área de desenho -- O utilizador pode definir quais os itens da GUI "
"que ficarão escondidos no modo apenas da área de desenho. Os itens "
"seleccionados ficarão escondidos."
