# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-18 09:38+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Little en KritaPreferencesColorManagement iccprofiles\n"
"X-POFile-SpellExtra: Krita ref image icc share colormanagedworkflow ICC\n"
"X-POFile-SpellExtra: menuselection sRGB usr CPP images LittleCMS CMS XYZ\n"
"X-POFile-SpellExtra: color LCMS preferences\n"

#: ../../reference_manual/preferences/color_management_settings.rst:1
msgid "The color management settings in Krita."
msgstr "A configuração da gestão de cores no Krita."

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Preferences"
msgstr "Preferências"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Color Management"
msgstr "Gestão de Cores"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Softproofing"
msgstr "Prova suave"

#: ../../reference_manual/preferences/color_management_settings.rst:17
msgid "Color Management Settings"
msgstr "Configuração da Gestão de Cores"

#: ../../reference_manual/preferences/color_management_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Color_Management.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Color_Management.png"

#: ../../reference_manual/preferences/color_management_settings.rst:21
msgid ""
"Krita offers extensive functionality for color management, utilising `Little "
"CMS <http://www.littlecms.com/>`_ We describe Color Management in a more "
"overall level here: :ref:`color_managed_workflow`."
msgstr ""
"O Krita oferece uma funcionalidade alargada para a gestão de cores, usando o "
"`Little CMS <http://www.littlecms.com/>`_ Será descrita aqui a Gestão de "
"Cores a um nível mais global: :ref:`color_managed_workflow`."

#: ../../reference_manual/preferences/color_management_settings.rst:25
msgid "General"
msgstr "Geral"

#: ../../reference_manual/preferences/color_management_settings.rst:28
msgid "Default Color Model For New Images"
msgstr "Modelo de Cores Predefinido das Imagens Novas"

#: ../../reference_manual/preferences/color_management_settings.rst:30
msgid "Choose the default model you prefer for all your images."
msgstr "Escolha o modelo que prefere para as suas imagens novas."

#: ../../reference_manual/preferences/color_management_settings.rst:33
msgid "When Pasting Into Krita From Other Applications"
msgstr "Quando Colar no Krita a Partir de Outras Aplicações"

#: ../../reference_manual/preferences/color_management_settings.rst:35
msgid ""
"The user can define what kind of conversion, if any, Krita will do to an "
"image that is copied from other applications i.e. Browser, GIMP, etc."
msgstr ""
"O utilizador poderá definir qual o tipo de conversão, se necessário, que o "
"Krita irá fazer a uma imagem que seja copiada de outras aplicações, p.ex., "
"Navegador, GIMP, etc."

#: ../../reference_manual/preferences/color_management_settings.rst:37
msgid "Assume sRGB"
msgstr "Assumir o sRGB"

#: ../../reference_manual/preferences/color_management_settings.rst:38
msgid ""
"This option will show the pasted image in the default Krita ICC profile of "
"sRGB."
msgstr ""
"Esta opção irá mostrar a imagem colada no perfil de ICC predefinido do "
"Krita, que é o sRGB."

#: ../../reference_manual/preferences/color_management_settings.rst:39
msgid "Assume monitor profile"
msgstr "Assumir o perfil do monitor"

#: ../../reference_manual/preferences/color_management_settings.rst:40
msgid ""
"This option will show the pasted image in the monitor profile selected in "
"system preferences."
msgstr ""
"Esta opção irá mostrar a imagem colada no perfil do monitor seleccionado nas "
"preferências do sistema."

#: ../../reference_manual/preferences/color_management_settings.rst:42
msgid "Ask each time"
msgstr "Perguntar sempre"

#: ../../reference_manual/preferences/color_management_settings.rst:42
msgid ""
"Krita will ask the user each time an image is pasted, what to do with it. "
"This is the default."
msgstr ""
"O Krita irá perguntar ao utilizador, quando colar uma imagem, o que deve "
"fazer com ela. Este é o valor por omissão."

#: ../../reference_manual/preferences/color_management_settings.rst:46
msgid ""
"When copying and pasting in Krita color information is always preserved."
msgstr ""
"Ao copiar e colar no Krita, a informação das cores é sempre preservada."

#: ../../reference_manual/preferences/color_management_settings.rst:49
msgid "Use Blackpoint Compensation"
msgstr "Usar a Compensação do Ponto Negro"

#: ../../reference_manual/preferences/color_management_settings.rst:51
msgid ""
"This option will turn on Blackpoint Compensation for the conversion. BPC is "
"explained by the maintainer of LCMS as following:"
msgstr ""
"Esta opção irá activar a Compensação por Ponto Preto na conversão. O CPP é "
"explicada pelo responsável de manutenção do LCMS da seguinte forma:"

#: ../../reference_manual/preferences/color_management_settings.rst:53
msgid ""
"BPC is a sort of \"poor man's\" gamut mapping. It basically adjust contrast "
"of images in a way that darkest tone of source device gets mapped to darkest "
"tone of destination device. If you have an image that is adjusted to be "
"displayed on a monitor, and want to print it on a large format printer, you "
"should realize printer can render black significantly darker that the "
"screen. So BPC can do the adjustment for you. It only makes sense on "
"Relative colorimetric intent. Perceptual and Saturation does have an "
"implicit BPC."
msgstr ""
"A CPP é uma espécie de mapeamento do gamute \"de segunda categoria\". "
"Basicamente ajusta o contraste das imagens de forma a que o tom mais escuro "
"do dispositivo de origem fique associado ao tom mais escuro do dispositivo "
"de destino. Se tiver uma imagem que tenha sido ajustada para ser apresentada "
"num monitor, e caso a deseje imprimir numa impressora para grandes formatos, "
"deverá reparar que a impressora consegue desenhar o preto de forma mais "
"escura do que no ecrã. Como tal, o CPP poderá fazer o ajuste por si. Só faz "
"sentido na tentativa de desenho colorimétrica Relativa. A Percepção e a "
"Saturação têm um CPP implícito."

#: ../../reference_manual/preferences/color_management_settings.rst:56
msgid "Allow LittleCMS optimizations"
msgstr "Permitir as optimizações do LittleCMS"

#: ../../reference_manual/preferences/color_management_settings.rst:58
msgid "Uncheck this option when using Linear Light RGB or XYZ."
msgstr "Desligue esta opção ao usar o RGB de Luz Linear ou o XYZ."

#: ../../reference_manual/preferences/color_management_settings.rst:61
msgid "Display"
msgstr "Visualização"

#: ../../reference_manual/preferences/color_management_settings.rst:63
msgid "Use System Monitor Profile"
msgstr "Usar o Perfil de Monitor do Sistema"

#: ../../reference_manual/preferences/color_management_settings.rst:64
msgid ""
"This option when selected will tell Krita to use the ICC profile selected in "
"your system preferences."
msgstr ""
"Esta opção, quando estiver seleccionada, irá indicar ao Krita para usar o "
"perfil de ICC seleccionado na configuração do seu sistema."

#: ../../reference_manual/preferences/color_management_settings.rst:65
msgid "Screen Profiles"
msgstr "Perfis do Ecrã"

#: ../../reference_manual/preferences/color_management_settings.rst:66
msgid ""
"There are as many of these as you have screens connected. The user can "
"select an ICC profile which Krita will use independent of the monitor "
"profile set in system preferences. The default is sRGB built-in. On Unix "
"systems, profile stored in $/usr/share/color/icc (system location) or $~/."
"local/share/color/icc (local location) will be proposed. Profile stored in "
"Krita preference folder, $~/.local/share/krita/profiles will be visible only "
"in Krita."
msgstr ""
"Existem tantos quantos monitores tiver ligados. O utilizador poderá "
"seleccionar um perfil do ICC para o Krita usar, independentemente do perfil "
"do monitor definido na configuração do sistema. Por omissão é o sRGB "
"incorporado. Nos sistemas Unix, o perfil pode estar gravado em $/usr/share/"
"color/icc (localização do sistema) ou em $~/.local/share/color/icc "
"(localização no utilizador), sendo este o valor proposto. O perfil gravado "
"na pasta de preferências do Krita, a $~/.local/share/krita/profiles, só será "
"visível no Krita."

#: ../../reference_manual/preferences/color_management_settings.rst:68
msgid "Rendering Intent"
msgstr "Tentativa de Desenho"

#: ../../reference_manual/preferences/color_management_settings.rst:68
msgid ""
"Your choice of rendering intents is a way of telling Littlecms how you want "
"colors mapped from one color space to another. There are four options "
"available, all are explained on the :ref:`icc_profiles` manual page."
msgstr ""
"A sua escolha das tentativas de desenho é uma forma de dizer ao LittleCMS "
"como deseja associar as cores de um espaço ao outro. Existem quatro opções "
"disponíveis, estando todas elas explicadas na página de manual do :ref:"
"`icc_profiles`."

#: ../../reference_manual/preferences/color_management_settings.rst:71
msgid "Softproofing options"
msgstr "Opções de prova suave"

#: ../../reference_manual/preferences/color_management_settings.rst:73
msgid ""
"These allow you to configure the *default* softproofing options. To "
"configure the actual softproofing for the current image, go to :"
"menuselection:`Image --> Image Properties --> Softproofing` ."
msgstr ""
"Estas permitem-lhe configurar as opções de prova *predefinidas*. Para "
"configurar a prova actual para a imagem aberta de momento, vá a :"
"menuselection:`Imagem --> Propriedades da Imagem --> Prova Suave` ."

#: ../../reference_manual/preferences/color_management_settings.rst:75
msgid ""
"For indepth details about how to use softproofing, check out :ref:`the page "
"on softproofing <soft_proofing>`."
msgstr ""
"Para detalhes mais aprofundados sobre como usar esta prova, veja a :ref:"
"`página sobre a prova suave <soft_proofing>`."
