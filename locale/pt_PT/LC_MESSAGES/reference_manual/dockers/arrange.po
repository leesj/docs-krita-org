# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-18 13:03+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: ref shapeselectiontool\n"

#: ../../<generated>:1
msgid "Grouping"
msgstr "Agrupamento"

#: ../../reference_manual/dockers/arrange.rst:1
msgid "The arrange docker."
msgstr "A barra de arrumação."

#: ../../reference_manual/dockers/arrange.rst:14
msgid "Arrange"
msgstr "Arrumar"

#: ../../reference_manual/dockers/arrange.rst:16
msgid ""
"A docker for aligning and arranging vector shapes. When you have the :ref:"
"`shape_selection_tool` active, the following actions will appear on this "
"docker:"
msgstr ""
"Uma área para alinhar e organizar as formas vectoriais. Quando tiver a :ref:"
"`shape_selection_tool` activa, as seguintes acções irão aparecer nesta área:"

#: ../../reference_manual/dockers/arrange.rst:19
msgid "Align all selected objects."
msgstr "Alinhar todos os objectos seleccionados."

#: ../../reference_manual/dockers/arrange.rst:21
msgid "Align Left"
msgstr "Alinhar à Esquerda"

#: ../../reference_manual/dockers/arrange.rst:22
msgid "Horizontally Center"
msgstr "Centrar na Horizontal"

#: ../../reference_manual/dockers/arrange.rst:23
msgid "Align Right"
msgstr "Alinhar à Direita"

#: ../../reference_manual/dockers/arrange.rst:24
msgid "Align Top"
msgstr "Alinhar ao Topo"

#: ../../reference_manual/dockers/arrange.rst:25
msgid "Vertically Center"
msgstr "Centrar na Vertical"

#: ../../reference_manual/dockers/arrange.rst:26
msgid "Align"
msgstr "Alinhar"

#: ../../reference_manual/dockers/arrange.rst:26
msgid "Align Bottom"
msgstr "Alinhar ao Fundo"

#: ../../reference_manual/dockers/arrange.rst:29
msgid "Ensure that objects are distributed evenly."
msgstr "Garante que os objectos estão distribuídos de forma uniforme."

#: ../../reference_manual/dockers/arrange.rst:31
msgid "Distribute left edges equidistantly."
msgstr "Distribui os extremos esquerdos com a mesma distância."

#: ../../reference_manual/dockers/arrange.rst:32
msgid "Distribute centers equidistantly horizontally."
msgstr "Distribui os centros com a mesma distância na horizontal."

#: ../../reference_manual/dockers/arrange.rst:33
msgid "Distribute right edges equidistantly."
msgstr "Distribui os extremos direitos com a mesma distância."

#: ../../reference_manual/dockers/arrange.rst:34
msgid "Distribute top edges equidistantly."
msgstr "Distribui os extremos superiores com a mesma distância."

#: ../../reference_manual/dockers/arrange.rst:35
msgid "Distribute centers equidistantly vertically."
msgstr "Distribui os centros com a mesma distância na vertical."

#: ../../reference_manual/dockers/arrange.rst:36
msgid "Distribute"
msgstr "Distribuir"

#: ../../reference_manual/dockers/arrange.rst:36
msgid "Distribute bottom edges equidistantly."
msgstr "Distribui os extremos inferiores com a mesma distância."

#: ../../reference_manual/dockers/arrange.rst:39
msgid "Ensure the gaps between objects are equal."
msgstr "Garante que os intervalos entre os objectos são iguais."

#: ../../reference_manual/dockers/arrange.rst:41
msgid "Make horizontal gaps between object equal."
msgstr "Garante que os intervalos horizontais entre os objectos são iguais."

#: ../../reference_manual/dockers/arrange.rst:42
msgid "Spacing"
msgstr "Espaço"

#: ../../reference_manual/dockers/arrange.rst:42
msgid "Make vertical gaps between object equal."
msgstr "Garante que os intervalos verticais entre os objectos são iguais."

#: ../../reference_manual/dockers/arrange.rst:45
msgid "Change the order of vector objects."
msgstr "Muda a ordem dos objectos vectoriais."

#: ../../reference_manual/dockers/arrange.rst:47
msgid "Bring to front"
msgstr "Enviar para a frente"

#: ../../reference_manual/dockers/arrange.rst:48
msgid "Raise"
msgstr "Elevar"

#: ../../reference_manual/dockers/arrange.rst:49
msgid "Lower"
msgstr "Baixar"

#: ../../reference_manual/dockers/arrange.rst:50
msgid "Order"
msgstr "Ordem"

#: ../../reference_manual/dockers/arrange.rst:50
msgid "Bring to back"
msgstr "Enviar para trás"

#: ../../reference_manual/dockers/arrange.rst:53
msgid "Buttons to group and ungroup vector objects."
msgstr "Botões para agrupar e desagrupar os objectos vectoriais."
