# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:14+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: filter en Motion image Blur Gaussian Lens Kiki blur\n"
"X-POFile-SpellExtra: images filters\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/filters/Lens-blur-filter.png"
msgstr ".. image:: images/filters/Lens-blur-filter.png"

#: ../../reference_manual/filters/blur.rst:1
msgid "Overview of the blur filters."
msgstr "Introdução aos filtros de borrão."

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:15
#: ../../reference_manual/filters/blur.rst:39
msgid "Blur"
msgstr "Borrar"

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:25
msgid "Gaussian Blur"
msgstr "Borrão Gaussiano"

#: ../../reference_manual/filters/blur.rst:10
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/filters/blur.rst:17
msgid ""
"The blur filters are used to smoothen out the hard edges and details in the "
"images. The resulting image is blurry. below is an example of a blurred "
"image. The image of Kiki on right is the result of blur filter applied to "
"the image on left."
msgstr ""
"Os filtros de borrão são usados para suavizar as arestas e detalhes vincados "
"nas imagens. A imagem resultante ficará um pouco mais difusa. Em baixo, "
"encontra-se um exemplo de uma imagem borrada. A imagem da Kiki à direita é o "
"resultado da aplicação do filtro de borrão à imagem da esquerda."

#: ../../reference_manual/filters/blur.rst:21
msgid ".. image:: images/filters/Blur.png"
msgstr ".. image:: images/filters/Blur.png"

#: ../../reference_manual/filters/blur.rst:22
msgid "There are many different filters for blurring:"
msgstr "Existem diversos filtros diferentes para borrões:"

#: ../../reference_manual/filters/blur.rst:27
msgid ""
"You can input the horizontal and vertical radius for the amount of blurring "
"here."
msgstr ""
"Poderá introduzir aqui o raio horizontal e vertical para a quantidade de "
"borrão."

#: ../../reference_manual/filters/blur.rst:30
msgid ".. image:: images/filters/Gaussian-blur.png"
msgstr ".. image:: images/filters/Gaussian-blur.png"

#: ../../reference_manual/filters/blur.rst:32
msgid "Motion Blur"
msgstr "Borrão por Movimento"

#: ../../reference_manual/filters/blur.rst:34
msgid ""
"Doesn't only blur, but also subtly smudge an image into a direction of the "
"specified angle thus giving a feel of motion to the image. This filter is "
"often used to create effects of fast moving objects."
msgstr ""
"Não só borra, mas também desvia subtilmente uma imagem numa dada direcção do "
"ângulo indicado, dando assim uma sensação de movimento à imagem. Este filtro "
"é normalmente usado para criar os efeitos de objectos em rápido movimento."

#: ../../reference_manual/filters/blur.rst:37
msgid ".. image:: images/filters/Motion-blur.png"
msgstr ".. image:: images/filters/Motion-blur.png"

#: ../../reference_manual/filters/blur.rst:41
msgid "This filter creates a regular blur."
msgstr "Este filtro cria um borrão normal."

#: ../../reference_manual/filters/blur.rst:44
msgid ".. image:: images/filters/Blur-filter.png"
msgstr ".. image:: images/filters/Blur-filter.png"

#: ../../reference_manual/filters/blur.rst:46
msgid "Lens Blur"
msgstr "Borrão de Lentes"

#: ../../reference_manual/filters/blur.rst:48
msgid "Lens Blur Algorithm."
msgstr "O algoritmo de borrão da lente."
