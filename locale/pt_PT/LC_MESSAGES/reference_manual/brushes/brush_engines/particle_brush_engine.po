# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-25 12:02+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: optionsizeparticle optionairbrush optionopacitynflow\n"
"X-POFile-SpellExtra: icons image particlebrush blendingmodes Dx Dy images\n"
"X-POFile-SpellExtra: ref\n"

#: ../../<generated>:1
msgid "Iterations"
msgstr "Iterações"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:1
msgid "The Particle Brush Engine manual page."
msgstr "A página de manual do Motor de Pincéis de Partículas."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:16
msgid "Particle Brush Engine"
msgstr "Motor de Pincel de Partículas"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Motor de Pincéis"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:19
msgid ".. image:: images/icons/particlebrush.svg"
msgstr ".. image:: images/icons/particlebrush.svg"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:20
msgid ""
"A brush that draws wires using parameters. These wires always get more "
"random and crazy over drawing distance. Gives very intricate lines best used "
"for special effects."
msgstr ""
"Um pincel que desenha fios com base nalguns parâmetros. Estes fios vão "
"ficando cada vez mais aleatórios e desordenados ao longo da distância de "
"desenho. Cria um conjunto de linhas emaranhadas que são melhor usadas para "
"os efeitos especiais."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:23
msgid "Options"
msgstr "Opções"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:24
msgid ":ref:`option_size_particle`"
msgstr ":ref:`option_size_particle`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:26
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:27
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:32
msgid "Brush Size"
msgstr "Tamanho do Pincel"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:34
msgid "Particles"
msgstr "Partículas"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:35
msgid "How many particles there's drawn."
msgstr "Quantas partículas estão desenhadas."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:36
msgid "Opacity Weight"
msgstr "Peso da Opacidade"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:37
msgid "The Opacity of all particles. Is influenced by the painting mode."
msgstr ""
"A Opacidade de todas as partículas. É influenciada pelo modo de pintura."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:38
msgid "Dx Scale (Distance X Scale)"
msgstr "Escala Dx (Escala da Distância em X)"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:39
msgid ""
"How much the horizontal cursor distance affects the placing of the pixel. Is "
"unstable on negative values. 1.0 is equal."
msgstr ""
"Quanto é que a distância horizontal do cursor afecta a colocação dos pixels. "
"É instável com valores negativos, enquanto o 1,0 fica igual."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:40
msgid "Dy Scale (Distance Y Scale)"
msgstr "Escala Dy (Escala da Distância em Y)"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:41
msgid ""
"How much the vertical cursor distance affects the placing of the pixel. Is "
"unstable on negative values. 1.0 is equal."
msgstr ""
"Quanto é que a distância vertical do cursor afecta a colocação dos pixels. É "
"instável com valores negativos, enquanto o 1,0 fica igual."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:42
msgid "Gravity"
msgstr "Gravidade"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:43
msgid ""
"Multiplies with the previous particle's position, to find the new particle's "
"position."
msgstr ""
"Multiplica-se com a posição da partícula anterior para determinar a posição "
"da nova partícula."

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:45
msgid ""
"The higher, the higher the internal acceleration is, with the furthest away "
"particle from the brush having the highest acceleration. This means that the "
"higher iteration is, the faster and more randomly a particle moves over "
"time, giving a messier result."
msgstr ""
"Quanto maior for o valor, maior será a aceleração interna, com a partícula "
"mais distante do pincel a ter a aceleração mais elevada. Isto significa que, "
"quanto maior for a iteração, mais rápido e mais aleatório será o movimento "
"de uma partícula, dando um resultado mais confuso."
