# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 10:02+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Krita demão image flow brushengineopacity images\n"
"X-POFile-SpellExtra: demãos KritaPixelBrushSettingsFlow\n"
"X-POFile-IgnoreConsistency: Flow\n"
"X-POFile-SpellExtra: tooloptionssettings gif brushes ref\n"
"X-POFile-SpellExtra: flowopacityadaptflowpreset\n"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:1
msgid "Opacity and flow in Krita."
msgstr "Opacidade e fluxo no Krita."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:27
msgid "Opacity"
msgstr "Opacidade"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:30
msgid "Flow"
msgstr "Fluxo"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
msgid "Transparency"
msgstr "Transparência"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:17
msgid "Opacity and Flow"
msgstr "Opacidade e Fluxo"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:19
msgid "Opacity and flow are parameters for the transparency of a brush."
msgstr ""
"A opacidade e o fluxo são parâmetros para a transparência de um pincel."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:22
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Flow.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Flow.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:23
msgid "They are interlinked with the painting mode setting."
msgstr "Estão interligados com a configuração do modo de pintura."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:26
msgid ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_02.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:28
msgid "The transparency of a stroke."
msgstr "A transparência de um dado traço."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:30
msgid ""
"The transparency of separate dabs. Finally separated from Opacity in 2.9."
msgstr ""
"A transparência das demãos em separado. Finalmente separada da Opacidade a "
"partir do 2.9."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:34
msgid ".. image:: images/brushes/Krita_4_2_brushengine_opacity-flow_01.svg"
msgstr ".. image:: images/brushes/Krita_4_2_brushengine_opacity-flow_01.svg"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:35
msgid ""
"In Krita 4.1 and below, the flow and opacity when combined with brush "
"sensors would add up to one another, being only limited by the maximum "
"opacity. This was unexpected compared to all other painting applications, so "
"in 4.2 this finally got corrected to the flow and opacity multiplying, "
"resulting in much more subtle strokes. This change can be switched back in "
"the :ref:`tool_options_settings`, but we will be deprecating the old way in "
"future versions."
msgstr ""
"No Krita 4.1 e inferiores, o fluxo e a opacidade, quando combinados com os "
"sensores dos pincéis, somar-se-iam entre si, sendo apenas limitada pela "
"opacidade máxima. Isto era inesperado em comparação com todas as outras "
"aplicações de pintura, pelo que no 4.2 isto finalmente foi corrigido na "
"multiplicação do fluxo e da opacidade, resultando em traços muito mais "
"subtis. Esta mudança poderá ser revertida nas :ref:`tool_options_settings`, "
"mas iremos descontinuar a forma antiga nas versões futuras."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:38
msgid "The old behavior can be simulated in the new system by..."
msgstr "O comportamento antigo pode ser simulado no novo sistema se..."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:40
msgid "Deactivating the sensors on opacity."
msgstr "Desactivar os sensores da opacidade."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:41
msgid "Set the maximum value on flow to 0.5."
msgstr "Configurar o valor máximo do fluxo como 0,5."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:42
msgid "Adjusting the pressure curve to be concave."
msgstr "Ajustar a curva de pressão para uma forma côncava."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:44
msgid ".. image:: images/brushes/flow_opacity_adapt_flow_preset.gif"
msgstr ".. image:: images/brushes/flow_opacity_adapt_flow_preset.gif"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:49
msgid "Painting mode"
msgstr "Modo de pintura"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:51
msgid "Build-up"
msgstr "Composição"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:52
msgid "Will treat opacity as if it were the same as flow."
msgstr "Irá tratar a opacidade como se fosse igual ao Fluxo."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:54
msgid "Wash"
msgstr "Lavagem"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:54
msgid "Will treat opacity as stroke transparency instead of dab-transparency."
msgstr ""
"Irá tratar a opacidade como uma transparência do traço em vez da "
"transparência da demão."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:57
msgid ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_03.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_03.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:58
msgid ""
"Where the other images of this page had all three strokes set to painting "
"mode: wash, this one is set to build-up."
msgstr ""
"onde as outras imagens tinham todos os três traços configurados com o modo "
"de pintura 'lavagem', este está configurado por 'composição'."
