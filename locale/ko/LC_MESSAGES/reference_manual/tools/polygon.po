# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Shinjo Park <kde@peremen.name>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-05-30 00:30+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:30
msgid ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"
msgstr ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"

#: ../../reference_manual/tools/polygon.rst:1
msgid "Krita's polygon tool reference."
msgstr "Krita의 다각형 도구 참조입니다."

#: ../../reference_manual/tools/polygon.rst:10
msgid "Tools"
msgstr "도구"

#: ../../reference_manual/tools/polygon.rst:10
msgid "Polygon"
msgstr "다각형"

#: ../../reference_manual/tools/polygon.rst:15
msgid "Polygon Tool"
msgstr "다각형 도구"

#: ../../reference_manual/tools/polygon.rst:17
msgid "|toolpolygon|"
msgstr "|toolpolygon|"

#: ../../reference_manual/tools/polygon.rst:19
#, fuzzy
#| msgid ""
#| "With this tool you can draw polygons. Click the |mouseleft| to indicate "
#| "the starting point and successive vertices, then double-click or press :"
#| "kbd:`Enter` to connect the last vertex to the starting point."
msgid ""
"With this tool you can draw polygons. Click the |mouseleft| to indicate the "
"starting point and successive vertices, then double-click or press the :kbd:"
"`Enter` key to connect the last vertex to the starting point."
msgstr ""
"이 도구를 사용하여 다각형을 그릴 수 있습니다. |mouseleft| 단추를 눌러서 시작 "
"꼭짓점과 계속해서 나오는 꼭짓점 위치를 지정할 수 있으며, 다각형 그리기를 끝내"
"려면 두 번 누르거나 :kbd:`Enter` 키를 누르면 끝 꼭짓점과 시작 꼭짓점을 선으"
"로 잇습니다."

#: ../../reference_manual/tools/polygon.rst:21
msgid ":kbd:`Shift + Z` undoes the last clicked point."
msgstr ":kbd:`Shift + Z` 키를 누르면 마지막으로 눌렀던 지점을 취소합니다."

#: ../../reference_manual/tools/polygon.rst:24
msgid "Tool Options"
msgstr "도구 옵션"
