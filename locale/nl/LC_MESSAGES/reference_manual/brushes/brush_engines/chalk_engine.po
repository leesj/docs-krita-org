# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-28 14:50+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:1
msgid "The Chalk Brush Engine manual page."
msgstr "De handleidingpagina van kalkpenseel-engine."

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:17
msgid "Chalk Brush Engine"
msgstr "Kalkpenseel-engine"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:21
msgid ""
"This brush engine has been removed in 4.0. There are other brush engines "
"such as pixel that can do everything this can...plus more."
msgstr ""
"Deze penseel-engine is verwijderd in 4.0. Er zijn andere penseel-engines "
"zoals pixel die alles als deze kunnen doen... plus meer."

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:23
msgid ""
"Apparently, the Bristle brush engine is derived from this brush engine. Now, "
"all of :program:`Krita's` brushes have a great variety of uses, so you must "
"have tried out the Chalk brush and wondered what it is for. Is it nothing "
"but a pixel brush with opacity and saturation fade options? As per the "
"developers this brush uses a different algorithm than the Pixel Brush, and "
"they left it in here as a simple demonstration of the capabilities of :"
"program:`Krita's` brush engines."
msgstr ""
"Kennelijk is de Borstel penseelengine afgeleid van deze penseelengine. Nu, "
"alle penselen van :program:`Krita` hebben een grote variëteit van gebruik, u "
"moet dus de Kalkpenseel hebben gebruikt en zich afgevraagd waar is het voor. "
"Is het niets anders dan een pixelpenseel met vervagingsopties voor dekking "
"en verzadiging? Volgens de ontwikkelaars gebruikt dit penseel een ander "
"algoritme dan het Pixelpenseel en ze hebben het hier in gelaten als een "
"eenvoudige demonstratie van de mogelijkheden van de penseelengines van :"
"program:`Krita`."

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:26
msgid ""
"So there you go, this brush is here for algorithmic demonstration purposes. "
"Don't lose sleep because you can't figure out what it's for, it Really "
"doesn't do much. For the sake of description, here's what it does:"
msgstr ""
"Dit is het dus, dit penseel is hier voor demonstratiedoeleinden van "
"algoritmen. Lig niet wakker omdat u niet begrijpt waar het voor is, in "
"werkelijkheid doet het niet veel. Om het toch maar te beschrijven is hier "
"wat het doet:"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:29
msgid ".. image:: images/brushes/Krita-tutorial7-C.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-C.png"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:30
msgid ""
"Yeah, that's it, a round brush with some chalky texture, and the option to "
"fade in opacity and saturation. That's it."
msgstr ""
"Ja, dat is het, een rond penseel met enige kalkachtige textuur en de optie "
"om dekking en verzadiging te laten vervagen. Dat is het."
