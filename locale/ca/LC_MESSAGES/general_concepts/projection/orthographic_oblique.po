# Translation of docs_krita_org_general_concepts___projection___orthographic_oblique.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-07 11:02+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_01.svg"
msgstr ".. image:: images/category_projection/projection-cube_01.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_02.svg"
msgstr ".. image:: images/category_projection/projection-cube_02.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_03.svg"
msgstr ".. image:: images/category_projection/projection-cube_03.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_04.svg"
msgstr ".. image:: images/category_projection/projection-cube_04.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_05.svg"
msgstr ".. image:: images/category_projection/projection-cube_05.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_06.svg"
msgstr ".. image:: images/category_projection/projection-cube_06.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_01.png"
msgstr ".. image:: images/category_projection/projection_image_01.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_02.png"
msgstr ".. image:: images/category_projection/projection_image_02.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_03.png"
msgstr ".. image:: images/category_projection/projection_image_03.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_04.png"
msgstr ".. image:: images/category_projection/projection_image_04.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_05.png"
msgstr ".. image:: images/category_projection/projection_image_05.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_06.png"
msgstr ".. image:: images/category_projection/projection_image_06.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_07.png"
msgstr ".. image:: images/category_projection/projection_image_07.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_08.png"
msgstr ".. image:: images/category_projection/projection_image_08.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_09.png"
msgstr ".. image:: images/category_projection/projection_image_09.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_10.png"
msgstr ".. image:: images/category_projection/projection_image_10.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_11.png"
msgstr ".. image:: images/category_projection/projection_image_11.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_12.png"
msgstr ".. image:: images/category_projection/projection_image_12.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_13.png"
msgstr ".. image:: images/category_projection/projection_image_13.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_14.png"
msgstr ".. image:: images/category_projection/projection_image_14.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_animation_01.gif"
msgstr ".. image:: images/category_projection/projection_animation_01.gif"

#: ../../general_concepts/projection/orthographic_oblique.rst:1
msgid "Orthographics and oblique projection."
msgstr "Projecció ortogràfica i obliqua."

#: ../../general_concepts/projection/orthographic_oblique.rst:10
msgid "So let's start with the basics..."
msgstr "Començarem amb els conceptes bàsics..."

#: ../../general_concepts/projection/orthographic_oblique.rst:12
#: ../../general_concepts/projection/orthographic_oblique.rst:16
msgid "Orthographic"
msgstr "Ortogràfica"

#: ../../general_concepts/projection/orthographic_oblique.rst:12
msgid "Projection"
msgstr "Projecció"

#: ../../general_concepts/projection/orthographic_oblique.rst:18
msgid ""
"Despite the fancy name, you probably know what orthographic is. It is a "
"schematic representation of an object, draw undeformed. Like the following "
"example:"
msgstr ""
"Malgrat el nom elegant, probablement conegueu què és ortogràfica. És una "
"representació esquemàtica d'un objecte, dibuix sense deformar. Com en el "
"següent exemple:"

#: ../../general_concepts/projection/orthographic_oblique.rst:23
msgid ""
"This is a rectangle. We have a front, top and side view. Put into "
"perspective it should look somewhat like this:"
msgstr ""
"Això és un rectangle. Tenim una vista frontal, superior i lateral. En "
"perspectiva, s'hauria de veure una cosa així:"

#: ../../general_concepts/projection/orthographic_oblique.rst:28
msgid ""
"While orthographic representations are kinda boring, they're also a good "
"basis to start with when you find yourself in trouble with a pose. But we'll "
"get to that in a bit."
msgstr ""
"Si bé les representacions ortogràfiques són una mica avorrides, també són "
"una bona base per a començar quan teniu problemes amb un posat. Però "
"arribarem a això en un moment."

#: ../../general_concepts/projection/orthographic_oblique.rst:33
msgid "Oblique"
msgstr "Obliqua"

#: ../../general_concepts/projection/orthographic_oblique.rst:35
msgid ""
"So, if we can say that the front view is the viewer looking at the front, "
"and the side view is the viewer directly looking at the side. (The "
"perpendicular line being the view plane it is projected on)"
msgstr ""
"Llavors, si podem dir que la vista frontal és l'observador mirant cap al "
"davant, i la vista lateral és l'observador mirant directament cap al costat. "
"(La línia perpendicular és el pla de vista sobre la qual es projecta)."

#: ../../general_concepts/projection/orthographic_oblique.rst:40
msgid "Then we can get a half-way view from looking from an angle, no?"
msgstr "Llavors obtenim una vista a mig camí des d'un angle, no?"

#: ../../general_concepts/projection/orthographic_oblique.rst:45
msgid "If we do that for a lot of different sides…"
msgstr "Si ho fem per a un munt de costats diferents..."

#: ../../general_concepts/projection/orthographic_oblique.rst:50
msgid "And we line up the sides we get a…"
msgstr "I si alineem els costats obtenim un..."

#: ../../general_concepts/projection/orthographic_oblique.rst:55
msgid ""
"But cubes are boring. I am suspecting that projection is so ignored because "
"no tutorial applies it to an object where you actually might NEED "
"projection. Like a face."
msgstr ""
"Però els cubs són avorrits. Sospito que la projecció és tan ignorada perquè "
"cap guia d'aprenentatge l'aplica a un objecte en el que realment es "
"NECESSITA una projecció. Com una cara."

#: ../../general_concepts/projection/orthographic_oblique.rst:57
msgid "First, let's prepare our front and side views:"
msgstr "Primer, preparem les nostres vistes frontal i lateral:"

#: ../../general_concepts/projection/orthographic_oblique.rst:62
msgid ""
"I always start with the side, and then extrapolate the front view from it. "
"Because you are using Krita, set up two parallel rulers, one vertical and "
"the other horizontal. To snap them perfectly, drag one of the nodes after "
"you have made the ruler, and press the :kbd:`Shift` key to snap it "
"horizontal or vertical. In 3.0, you can also snap them to the image borders "
"if you have :menuselection:`Snap Image Bounds` active via the :kbd:`Shift + "
"S` shortcut."
msgstr ""
"Sempre començament amb la lateral i després extrapolant des de la vista "
"frontal. Com que utilitzeu el Krita, establiu dos regles paral·lels, un de "
"vertical i l'altre horitzontal. Per ajustar-los a la perfecció, arrossegueu "
"un dels nodes després d'haver creat el regle i premeu la tecla :kbd:`Majús.` "
"per ajustar-lo de manera horitzontal o vertical. En la versió 3.0, també "
"podreu ajustar-los a les vores de la imatge si teniu activat :menuselection:"
"`Ajusta els límits de la imatge` mitjançant la drecera :kbd:`Majús. + S`."

#: ../../general_concepts/projection/orthographic_oblique.rst:64
msgid ""
"Then, by moving the mirror to the left, you can design a front view from the "
"side view, while the parallel preview line helps you with aligning the eyes "
"(which in the above screenshot are too low)."
msgstr ""
"Després, movent el mirall cap a l'esquerra, podreu dissenyar una vista "
"frontal des de la vista lateral, mentre que la línia de vista prèvia "
"paral·lela ajudarà a alinear els ulls (els quals a la captura de pantalla "
"anterior estan massa baixos)."

#: ../../general_concepts/projection/orthographic_oblique.rst:66
msgid "Eventually, you should have something like this:"
msgstr "De manera eventual, hauríeu de tenir alguna cosa així:"

#: ../../general_concepts/projection/orthographic_oblique.rst:71
msgid "And of course, let us not forget the top, it's pretty important:"
msgstr "I, per descomptat, no oblidem la part superior, és força important:"

#: ../../general_concepts/projection/orthographic_oblique.rst:78
msgid ""
"When you are using Krita, you can just use transform masks to rotate the "
"side view for drawing the top view."
msgstr ""
"Quan estigueu utilitzant el Krita, simplement podreu emprar màscares de "
"transformació per a girar la vista lateral i dibuixar la vista superior."

#: ../../general_concepts/projection/orthographic_oblique.rst:80
msgid ""
"The top view works as a method for debugging your orthos as well. If we take "
"the red line to figure out the orthographics from, we see that our eyes are "
"obviously too inset. Let's move them a bit more forward, to around the nose."
msgstr ""
"La vista superior també funciona com un mètode per a depurar les vostres "
"projeccions ortogràfiques. Si prenem la línia vermella per a descobrir el "
"punt de partida de la projecció ortogràfica, veiem que els nostres ulls "
"òbviament estan massa endins. Movem-los una mica més cap endavant, al "
"voltant del nas."

#: ../../general_concepts/projection/orthographic_oblique.rst:85
msgid ""
"If you want to do precision position moving in the tool options docker, just "
"select 'position' and the input box for the X. Pressing down then moves the "
"transformed selection left. With Krita 3.0 you can just use the move tool "
"for this and the arrow keys. Using transform here can be more convenient if "
"you also have to squash and stretch an eye."
msgstr ""
"Si voleu moure amb precisió la posició a l'acoblador Opcions de l'eina, "
"simplement seleccioneu «posició» i el quadre d'entrada per a la X. En prémer "
"cap avall, es mourà la selecció transformada cap a l'esquerra. Amb el Krita "
"3.0, simplement haureu d'utilitzar l'eina de moure i les tecles de fletxa "
"per a fer-ho. Utilitzar aquí la transformació pot ser més pràctic si també "
"heu d'aixafar i estirar un ull."

#: ../../general_concepts/projection/orthographic_oblique.rst:90
msgid "We fix the top view now. Much better."
msgstr "Ara hem arreglat la vista superior. Molt millor."

#: ../../general_concepts/projection/orthographic_oblique.rst:92
msgid ""
"For faces, the multiple slices are actually pretty important. So important "
"even, that I have decided we should have these slices on separate layers. "
"Thankfully, I chose to color them, so all we need to do is go to :"
"menuselection:`Layer --> Split Layer` ."
msgstr ""
"Per a les cares, les múltiples divisions són realment importants. Tan "
"important que he decidit que hauríem de tenir aquestes divisions en capes "
"separades. Afortunadament, he triat acolorir-les, de manera que tot el que "
"haurem de fer és anar a :menuselection:`Capa --> Divideix la capa`."

#: ../../general_concepts/projection/orthographic_oblique.rst:98
msgid ""
"This'll give you a few awkwardly named layers… rename them by selecting all "
"and mass changing the name in the properties editor:"
msgstr ""
"Això ens donarà unes quantes capes amb noms força incòmodes... canvieu-lis "
"el nom seleccionant-les totes i canviant el nom en massa a l'editor de les "
"propietats:"

#: ../../general_concepts/projection/orthographic_oblique.rst:103
msgid "So, after some cleanup, we should have the following:"
msgstr "Llavors, després d'alguna neteja, hauríem de tenir el següent:"

#: ../../general_concepts/projection/orthographic_oblique.rst:108
msgid "Okay, now we're gonna use animation for the next bit."
msgstr "D'acord, ara utilitzarem l'animació per al següent fragment."

#: ../../general_concepts/projection/orthographic_oblique.rst:110
msgid "Set it up as follows:"
msgstr "Configureu-la com segueix:"

#: ../../general_concepts/projection/orthographic_oblique.rst:115
msgid ""
"Both front view and side view are set up as 'visible in timeline' so we can "
"always see them."
msgstr ""
"Tant la vista frontal com la vista lateral estan configurades com a "
"«visibles a la línia de temps», de manera que sempre les podrem veure."

#: ../../general_concepts/projection/orthographic_oblique.rst:116
msgid ""
"Front view has its visible frame on frame 0 and an empty frame on frame 23."
msgstr ""
"La vista frontal té el seu fotograma visible en el fotograma 0 i un "
"fotograma buit en el fotograma 23."

#: ../../general_concepts/projection/orthographic_oblique.rst:117
msgid ""
"Side view has its visible frame on frame 23 and an empty view on frame 0."
msgstr ""
"La vista lateral té el seu fotograma visible en el fotograma 23 i una vista "
"buida en el fotograma 0."

#: ../../general_concepts/projection/orthographic_oblique.rst:118
msgid "The end of the animation is set to 23."
msgstr "El final de l'animació està establert al fotograma 23."

#: ../../general_concepts/projection/orthographic_oblique.rst:123
msgid ""
"Krita can't animate a transformation on multiple layers on multiple frames "
"yet, so let's just only transform the top layer. Add a semi-transparent "
"layer where we draw the guidelines."
msgstr ""
"El Krita encara no pot animar una transformació en múltiples capes en "
"múltiples fotogrames, de manera que només transformarem la capa superior. "
"Afegiu una capa semitransparent on dibuixarem les línies guia."

#: ../../general_concepts/projection/orthographic_oblique.rst:125
msgid ""
"Now, select frame 11 (halfway), add new frames from front view, side view "
"and the guidelines. And turn on the onion skin by toggling the lamp symbols. "
"We copy the frame for the top view and use the transform tool to rotate it "
"45°."
msgstr ""
"Ara, seleccioneu el fotograma 11 (a mig camí), afegiu fotogrames nous des de "
"la vista frontal, la vista lateral i les línies guia. I activeu la pell de "
"ceba alternant els símbols de llum. Copiarem el marc per a la vista superior "
"i utilitzarem l'eina de transformació per a girar-la 45°."

#: ../../general_concepts/projection/orthographic_oblique.rst:130
msgid "So, we draw our vertical guides again and determine a in-between..."
msgstr ""
"Llavors, dibuixem les nostres guies verticals novament i determinem els "
"fotogrames intermedis..."

#: ../../general_concepts/projection/orthographic_oblique.rst:135
msgid ""
"This is about how far you can get with only the main slice, so rotate the "
"rest as well."
msgstr ""
"Això tracta de fins on podreu arribar només amb la divisió principal, de "
"manera que gireu també la resta."

#: ../../general_concepts/projection/orthographic_oblique.rst:140
msgid "And just like with the cube, we do this for all slices…"
msgstr "I igual que amb el cub, farem això per a totes les divisions..."

#: ../../general_concepts/projection/orthographic_oblique.rst:145
msgid ""
"Eventually, if you have the top slices rotate every frame with 15°, you "
"should be able to make a turn table, like this:"
msgstr ""
"De manera eventual, si teniu les divisions superiors girades cada fotograma "
"en 15°, hauríeu de poder crear una taula de gir, com aquesta:"

#: ../../general_concepts/projection/orthographic_oblique.rst:150
msgid ""
"Because our boy here is fully symmetrical, you can just animate one side and "
"flip the frames for the other half."
msgstr ""
"Com que el nostre noi aquí és completament simètric, podreu animar un costat "
"i reflectir els fotogrames per a l'altra meitat."

#: ../../general_concepts/projection/orthographic_oblique.rst:152
msgid ""
"While it is not necessary to follow all the steps in the theory section to "
"understand the tutorial, I do recommend making a turn table sometime. It "
"teaches you a lot about drawing 3/4th faces."
msgstr ""
"Si bé no cal seguir tots els passos a la secció de teoria per a comprendre "
"la guia d'aprenentatge, recomano crear una taula de gir en algun moment. "
"Aquesta ensenya molt sobre com dibuixar tres quartes parts de les cares."

#: ../../general_concepts/projection/orthographic_oblique.rst:154
msgid "How about… we introduce the top view into the drawing itself?"
msgstr "Què passa amb... presentar la vista superior en el dibuix en si?"
