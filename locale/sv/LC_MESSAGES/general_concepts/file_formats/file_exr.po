# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:32+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_exr.rst:1
msgid "The EXR file format as exported by Krita."
msgstr "EXR-filformatet som exporteras av Krita."

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "EXR"
msgstr "EXR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "HDR Fileformat"
msgstr "HDR-filformat"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "OpenEXR"
msgstr "OpenEXR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "*.exr"
msgstr "*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:15
msgid "\\*.exr"
msgstr "\\*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:17
msgid ""
"``.exr`` is the prime file format for saving and loading :ref:`floating "
"point bit depths <bit_depth>`, and due to the library made to load and save "
"these images being fully open source, the main interchange format as well."
msgstr ""
"Filformatet ``.exr`` är huvudformatet för att spara och läsa in :ref: "
"`floating point bit depths <bit_depth>`, och på grund att biblioteket för "
"att läsa och spara bilderna är öppen källkod, är det också det huvudsakliga "
"utbytesformatet."

#: ../../general_concepts/file_formats/file_exr.rst:19
msgid ""
"Floating point bit-depths are used by the computer graphics industry to "
"record scene referred values, which can be made via a camera or a computer "
"renderer. Scene referred values means that the file can have values whiter "
"than white, which in turn means that such a file can record lighting "
"conditions, such as sunsets very accurately. These EXR files can then be "
"used inside a renderer to create realistic lighting."
msgstr ""
"Flyttalsbitdjup används av datorgrafikindustrin för att spela in "
"scenbaserade värden, som kan skapas via en kamera eller datoråtergivning. "
"Scenbaserade värden betyder att filen kan ha värden vitare än vitt, vilket i "
"sin tur betyder att en sådan fil kan spela in ljusförhållanden som "
"solnedgångar mycket noggrant. Sådana EXR-filer kan därefter användas inne i "
"ett återgivningsprogram för att skapa realistisk ljussättning."

#: ../../general_concepts/file_formats/file_exr.rst:21
msgid ""
"Krita can load and save EXR for the purpose of paint-over (yes, Krita can "
"paint with scene referred values) and interchange with applications like "
"Blender, Mari, Nuke and Natron."
msgstr ""
"Krita kan läsa in och spara EXR i överskrivningssyftet (ja, Krita kan måla "
"med scenbaserade värden) och utbyte med program som Blender, Mari, Nuke och "
"Natron."
