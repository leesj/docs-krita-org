# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:27+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/colors/bit_depth.rst:None
msgid ".. image:: images/color_category/Kiki_lowbit.png"
msgstr ".. image:: images/color_category/Kiki_lowbit.png"

#: ../../general_concepts/colors/bit_depth.rst:1
msgid "Bit depth in Krita."
msgstr "Bitdjup i Krita"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:15
msgid "Bit Depth"
msgstr "Bitdjup"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:26
msgid "Indexed Color"
msgstr "Indexerad färg"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:46
msgid "Real Color"
msgstr "Verklig färg"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color"
msgstr "Färg"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color Bit Depth"
msgstr "Färgbitdjup"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Deep Color"
msgstr "Djup färg"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Floating Point Color"
msgstr "Flyttalsfärg"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color Channels"
msgstr "Färgkanaler"

#: ../../general_concepts/colors/bit_depth.rst:17
msgid ""
"Bit depth basically refers to the amount of working memory per pixel you "
"reserve for an image."
msgstr ""
"Bitdjup anger i huvudsak hur mycket arbetsminne per bildpunkt som reserveras "
"för en bild."

#: ../../general_concepts/colors/bit_depth.rst:19
msgid ""
"Like how having a A2 paper in real life can allow for much more detail in "
"the end drawing, it does take up more of your desk than a simple A4 paper."
msgstr ""
"Som hur att använda ett A2-papper i verkligheten kan tillåta mycket fler "
"detaljer på den slutliga teckningen, men upptar mycket mer utrymme på "
"skrivbordet än ett vanligt A4-papper."

#: ../../general_concepts/colors/bit_depth.rst:21
msgid ""
"However, this does not just refer to the size of the image, but also how "
"much precision you need per color."
msgstr ""
"Dock gäller detta inte bara bildstorleken, men också hur mycket precision "
"som behövs per färg."

#: ../../general_concepts/colors/bit_depth.rst:23
msgid ""
"To illustrate this, I'll briefly talk about something that is not even "
"available in Krita:"
msgstr ""
"För att illustrera det, beskriver jag kortfattat något som inte ens är "
"tillgängligt i Krita:"

#: ../../general_concepts/colors/bit_depth.rst:28
msgid ""
"In older programs, the computer would have per image, a palette that "
"contains a number for each color. The palette size is defined in bits, "
"because the computer can only store data in bit-sizes."
msgstr ""
"I äldre program har datorn en palett per bild, som innehåller ett tal för "
"varje färg. Palettstorleken definieras i bitar, eftersom datorn bara kan "
"lagra data i bitstorlekar."

#: ../../general_concepts/colors/bit_depth.rst:36
msgid "1bit"
msgstr "1 bit"

#: ../../general_concepts/colors/bit_depth.rst:37
msgid "Only two colors in total, usually black and white."
msgstr "Bara totalt två färger, oftast svart och vitt."

#: ../../general_concepts/colors/bit_depth.rst:38
msgid "4bit (16 colors)"
msgstr "4 bitar (16 färger)"

#: ../../general_concepts/colors/bit_depth.rst:39
msgid ""
"16 colors in total, these are famous as many early games were presented in "
"this color palette."
msgstr ""
"Totalt 16 färger, som är berömda eftersom många tidiga spel använde sig av "
"den här färgpaletten."

#: ../../general_concepts/colors/bit_depth.rst:41
msgid "8bit"
msgstr "8 bitar"

#: ../../general_concepts/colors/bit_depth.rst:41
msgid ""
"256 colors in total. 8bit images are commonly used in games to save on "
"memory for textures and sprites."
msgstr ""
"Totalt 256 färger. 8-bitars bilder används ofta i spel för att spara på "
"minne för strukturer och figurer."

#: ../../general_concepts/colors/bit_depth.rst:43
msgid ""
"However, this is not available in Krita. Krita instead works with channels, "
"and counts how many colors per channel you need (described in terms of "
"''bits per channel''). This is called 'real color'."
msgstr ""
"Dock är det inte tillgängligt i Krita. Krita arbetar istället med kanaler, "
"och räknar hur många färger per kanal man behöver (som beskrivs i termer av "
"\"bitar per kanal\"). Det kallas 'verklig färg'."

#: ../../general_concepts/colors/bit_depth.rst:52
msgid ".. image:: images/color_category/Rgbcolorcube_3.png"
msgstr ".. image:: images/color_category/Rgbcolorcube_3.png"

#: ../../general_concepts/colors/bit_depth.rst:52
msgid ""
"1, 2, and 3bit per channel don't actually exist in any graphics application "
"out there, however, by imagining them, we can imagine how each bit affects "
"the precision: Usually, each bit subdivides each section in the color cube "
"meaning precision becomes a power of 2 bigger than the previous cube."
msgstr ""
"1, 2 och 3 bitar per kanal existerar i själva verket inte i något "
"grafikprogram i verkligheten. Vi kan dock föreställa oss hur varje bit "
"påverkar precisionen genom att avbilda dem. Oftast delar varje bit upp varje "
"sektion av färgkuben, vilket betyder att precisionen blir en tvåpotens "
"större en den föregående kuben."

#: ../../general_concepts/colors/bit_depth.rst:54
msgid "4bit per channel (not supported by Krita)"
msgstr "4-bitar per kanal (stöds inte av Krita)"

#: ../../general_concepts/colors/bit_depth.rst:55
msgid ""
"Also known as Hi-Color, or 16bit color total. A bit of an old system, and "
"not used outside of specific displays."
msgstr ""
"Också känd som Hi-Color, eller 16-bitars total färg. Ett något gammaldags "
"system, och används inte utom av särskilda bildskärmar."

#: ../../general_concepts/colors/bit_depth.rst:56
msgid "8bit per channel"
msgstr "8 bitar per kanal"

#: ../../general_concepts/colors/bit_depth.rst:57
msgid ""
"Also known as \"True Color\", \"Millions of colors\" or \"24bit/32bit\". The "
"standard for many screens, and the lowest bit-depth Krita can handle."
msgstr ""
"Också känt som \"True Color\", \"Miljontals färger\" eller \"24-bitar/32-"
"bitar\". Standard för många skärmar, och det minsta bitdjup som Krita kan "
"hantera."

#: ../../general_concepts/colors/bit_depth.rst:58
msgid "16bit per channel"
msgstr "16 bitar per kanal"

#: ../../general_concepts/colors/bit_depth.rst:59
msgid ""
"One step up from 8bit, 16bit per channel allows for colors that can't be "
"displayed by the screen. However, due to this, you are more likely to have "
"smoother gradients. Sometimes known as \"Deep Color\". This color depth type "
"doesn't have negative values possible, so it is 16bit precision, meaning "
"that you have 65536 values per channel."
msgstr ""
"Ett steg upp från 8 bitar, tillåter 16 bitar per kanal färger som inte kan "
"visas på skärmen. På grund av det är det dock troligare att ha jämnare "
"toningar. Det kallas ibland för \"djup färg\". Typen av färgdjup tillåter "
"inte negativa värden, så det är 16-bitars precision, vilket betyder att man "
"har 65536 värden per kanal."

#: ../../general_concepts/colors/bit_depth.rst:60
msgid "16bit float"
msgstr "16-bitars flyttal"

#: ../../general_concepts/colors/bit_depth.rst:61
msgid ""
"Similar to 16bit, but with more range and less precision. Where 16bit only "
"allows coordinates like [1, 4, 3], 16bit float has coordinates like [0.15, "
"0.70, 0.3759], with [1.0,1.0,1.0] representing white. Because of the "
"differences between floating point and integer type variables, and because "
"Scene-referred imaging allows for negative values, you have about 10-11bits "
"of precision per channel in 16 bit floating point compared to 16 bit in 16 "
"bit int (this is 2048 values per channel in the 0-1 range). Required for HDR/"
"Scene referred images, and often known as 'half floating point'."
msgstr ""
"Liknar 16 bitar, men har större intervall och mindre precision. Medan 16 "
"bitar bara tillåter koordinater som [1; 4; 3], har 16-bitars flyttal "
"koordinater som [0,15; 0,70; 0,3759], där [1,0; 1,0; 1,0] representerar "
"vitt. På grund av skillnaden mellan flyttals- och heltalsvariabler, och "
"eftersom scenbaserad avbildning tillåter negativa värden, kan man ha omkring "
"10-11 bitars precision per kanal med 16-bitars flyttal jämfört med 16 bitar "
"i 16-bitars heltal (det vill säga 2048 värden per kanal i intervallet 0-1). "
"Krävs för HDR och scenbaserade bilder, och kallas ofta för 'halv precision'."

#: ../../general_concepts/colors/bit_depth.rst:63
msgid ""
"Similar to 16bit float but with even higher precision. The native color "
"depth of OpenColor IO, and thus faster than 16bit float in HDR images, if "
"not heavier. Because of the nature of floating point type variables, 32bit "
"float is roughly equal to 23-24 bits of precision per channel (16777216 "
"values per channel in the 0-1 range), but with a much wider range (it can go "
"far above 1), necessary for HDR/Scene-referred values. It is also known as "
"'single floating point'."
msgstr ""
"Liknar 16-bitars flyttal, men med ännu större precision. Det inbyggda "
"färgdjupet i OpenColor IO, och därmed snabbare än 16-bitars flyttal i HDR-"
"bilder, om inte mer krävande. På grund av flyttalsvariablers natur, 32-"
"bitars flyttal är ungefär lika med 23-24 bitars precision per kanal "
"(16777216 värden per kanal i intervallet 0-1), men med ett mycket större "
"intervall (det kan vara mycket större än 1), nödvändigt för HDR eller "
"scenbaserade bilder, och kallas ofta för 'enkel precision'."

#: ../../general_concepts/colors/bit_depth.rst:64
msgid "32bit float"
msgstr "32-bitars flyttal"

#: ../../general_concepts/colors/bit_depth.rst:66
msgid ""
"This is important if you have a working color space that is larger than your "
"device space: At the least, if you do not want color banding."
msgstr ""
"Det är viktigt om man har en arbetsrymd som är större än enhetens färgrymd: "
"Åtminstone om man vill undvika färgbandning."

#: ../../general_concepts/colors/bit_depth.rst:68
msgid ""
"And while you can attempt to create all your images a 32bit float, this will "
"quickly take up your RAM. Therefore, it's important to consider which bit "
"depth you will use for what kind of image."
msgstr ""
"Och även om man kan försöka skapa alla sina bilder med 32-bitars flyttal, "
"använder det snabbt allt minne. Därför är det viktigt att tänka ut vilket "
"bitdjup man ska använda för vilken sorts bild."
