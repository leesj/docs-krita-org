msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-02-27 03:16+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/resource_management/paintoppresets.rst:1
msgid "The Brush Presets resource in Krita."
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:11
msgid "Resources"
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:11
#, fuzzy
#| msgid "Brushes"
msgid "Brush Presets"
msgstr "Brosses"

#: ../../reference_manual/resource_management/paintoppresets.rst:11
msgid "Brushes"
msgstr "Brosses"

#: ../../reference_manual/resource_management/paintoppresets.rst:11
msgid "Paintop Presets"
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:16
msgid "Brush Preset"
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:18
msgid ""
"Paint Op presets store the preview thumbnail, brush-engine, the parameters, "
"the brush tip, and, if possible, the texture. They are saved as .kpp files"
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:20
msgid ""
"For information regarding the brush system, see :ref:`Brushes "
"<loading_saving_brushes>`."
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:23
msgid "The Docker"
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:25
msgid ""
"The docker for Paint-op presets is the :ref:`brush_preset_docker`. Here you "
"can tag, add, remove and search paint op presets."
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:28
msgid "Editing the preview thumbnail"
msgstr ""

#: ../../reference_manual/resource_management/paintoppresets.rst:30
msgid ""
"You can edit the preview thumbnail in the brush-scratchpad, but you can also "
"open the \\*.kpp file in Krita to get a 200x200 file to edit to your wishes."
msgstr ""
